from gomoddepviz import IdGen


def test_next():
    ig = IdGen()
    assert ig.next() == "A"
    for _ in range(24):
        ig.next()
    assert ig.next() == "Z"
    assert ig.next() == "A1"


def test_get():
    ig = IdGen()
    for _ in range(200):
        ig.get("foo") == "A"
        ig.get("bar") == "B"
