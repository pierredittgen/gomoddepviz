#!/usr/bin/env python3
"""
gomoddepviz

Generate a graphviz dot dependency chart from go.mod file(s)
"""
import argparse
import fileinput
import logging
import sys
from collections import defaultdict
from dataclasses import dataclass
from itertools import cycle
from pathlib import Path
from typing import Iterator, List, Dict

log = logging.getLogger(__name__)


def iter_gomod_content(input: Iterator[str]) -> Iterator[str]:
    """Turn a list of file path to a list of go mod content."""
    for line in input:
        line = line.strip()
        if not line.endswith("go.mod"):
            continue
        gomod_path = Path(line)
        if not gomod_path.exists():
            log.error("File %r not found", str(gomod_path))
            continue
        yield gomod_path.read_text()


@dataclass
class GoRef:
    ref_name: str
    ref_version: str
    ref_comment: str


@dataclass
class GoMod:
    module_name: str
    go_version: str
    dependencies: List[GoRef]


def parse_dependency(input: str) -> GoRef:
    chunks = input.split(" ")
    ref_name = chunks[0]
    ref_version = chunks[1]
    comment = "" if len(chunks) < 4 else chunks[3]
    return GoRef(ref_name=ref_name, ref_version=ref_version, ref_comment=comment)


def parse_gomod(gomod_content: str) -> GoMod:
    go_version = ""
    module_name = ""
    dependencies: List[GoRef] = []
    in_dependency = False
    for line in gomod_content.split("\n"):
        line = line.strip()
        if line.startswith("module "):
            module_name = line[6:].strip()
            continue
        if line.startswith("go "):
            go_version = line[3:].strip()
            continue
        if line.startswith("require ("):
            in_dependency = True
            continue
        if line == ")" and in_dependency:
            in_dependency = False
            continue
        if line:
            if line.startswith("require "):
                dependencies.append(parse_dependency(line[8:].strip()))
            if in_dependency:
                dependencies.append(parse_dependency(line))

    return GoMod(
        go_version=go_version, module_name=module_name, dependencies=dependencies
    )


@dataclass
class Node:
    name: str
    dependencies: List[str]


def gomod_to_node(gomod: GoMod, prefix:str=None) -> Node:
    """Convert GoMod instance to Node instance, stripping prefix from node names."""
    def remove_prefix(mod_name: str):
        return mod_name[len(prefix):] if prefix and mod_name.startswith(prefix) else mod_name
    str_dependencies = [remove_prefix(dep.ref_name) for dep in gomod.dependencies]
    return Node(name=remove_prefix(gomod.module_name), dependencies=str_dependencies)


class Graph:
    def __init__(self, nodes: List[Node], extended=True):
        self.nodes = set([node.name for node in nodes])
        if extended:
            for node in nodes:
                self.nodes.update(node.dependencies)
        self.edges = defaultdict(set)
        for node in nodes:
            dependencies = [ref for ref in node.dependencies if ref in self.nodes]
            if dependencies:
                self.edges[node.name].update(dependencies)


class IdGen:
    """Generates identifier : A, B, ... Z, A1, B1, ... Z1, A2, ..."""

    def __init__(self):
        self.letter_provider = cycle(list("ABCDEFGHIJKLMNOPQRSTUVWXYZ"))
        self.num = 0
        self.store: Dict[str, str] = {}

    def next(self) -> str:
        letter = next(self.letter_provider)
        id_ = letter + (str(self.num) if self.num else "")
        if letter == "Z":
            self.num += 1
        return id_

    def get(self, name: str) -> str:
        if name not in self.store:
            self.store[name] = self.next()
        return self.store[name]


def graph_to_dot(g: Graph) -> str:
    gen = IdGen()

    # Header
    lines: List[str] = []
    lines.append("digraph G {")

    # nodes
    for node_name in g.nodes:
        lines.append(f'{gen.get(node_name)} [label="{node_name}"]')

    # edges
    for node_name, dependencies in g.edges.items():
        source = gen.get(node_name)
        targets = [gen.get(elt) for elt in dependencies]
        if targets:
            lines.append("%s -> { %s }" % (source, " ".join(targets)))

    # footer
    lines.append("}")

    return "\n".join(lines)

HTML_TPL = """
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<style>
		#dot-source { display: none; }
	</style>
</head>
<body>

<pre id="dot-source">
%s
</pre>
<div id="graph" style="text-align: center;"></div>

<script src="https://d3js.org/d3.v5.min.js"></script>
<script src="https://unpkg.com/@hpcc-js/wasm@0.3.11/dist/index.min.js"></script>
<script src="https://unpkg.com/d3-graphviz@3.0.5/build/d3-graphviz.js"></script>
<script>
d3.select("#graph").graphviz()
    .renderDot(document.getElementById('dot-source').innerText);
</script>
</body>
</html>
"""

def dot_to_html(dot_content: str) -> str:
    return HTML_TPL % dot_content

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-e", "--extended", help="include external dependencies", action="store_true"
    )
    parser.add_argument(
        "-p", "--module-prefix", type=str, help="remove prefix from module names"
    )
    parser.add_argument(
        "--html", help="generate HTML file embedding graph (using d3-graphviz)", action="store_true"
    )
    parser.add_argument(
        "files",
        metavar="FILE",
        nargs="*",
        help="go.mod files to read, if empty, stdin is used",
    )
    args = parser.parse_args()

    nodes = []
    for gomod_text in iter_gomod_content(
        fileinput.input(files=args.files if args.files else ("-",))
    ):
        gomod = parse_gomod(gomod_text)
        nodes.append(gomod_to_node(gomod, prefix=args.module_prefix))
    g = Graph(nodes, extended=args.extended)

    dot_content = graph_to_dot(g)
    if args.html:
        print(dot_to_html(dot_content))
    else:
        print(dot_content)


if __name__ == "__main__":
    sys.exit(main())
