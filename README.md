# Gomoddepviz

Generate [graphviz dot format](https://graphviz.org/docs/layouts/dot/) content from a bunch of [go.mod](https://go.dev/doc/modules/gomod-ref) files

Gomoddepviz parses `require` information from `go.mod` files to extract go module dependencies (inter modules and external dependencies).

This is beta software; use at your own risk!

- Pure Python script, no dependency.
- Install `graphviz` package to view graph.

Typical use:

```bash
# Generate inter-modules dependency graph
find /my/project -name 'go.mod' | python gomoddepviz.py > my_project.dot
# or
python gomodviz.py -- /my/project/*/go.mod > my_project.dot
```

See `python gomodviz.py --help` for more options

## Generate an image from a dot file

Using graphviz `dot` tool.

E.g. using SVG format:

```bash
dot my_project.dot -Tsvg -o my_project.svg
```

Or use [d3-graphviz](https://github.com/magjac/d3-graphviz#examples) to display your graph on the web!
